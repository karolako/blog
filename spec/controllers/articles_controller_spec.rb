require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do

  let(:valid_attributes) {
    {
        :username => "Foo",
        :email => "foo@bar.com",
        :password => "password",
        :password_confirmation => "password"
    }
  }
  let(:params) { {
      title: 'Awesome article',
      description: 'Description'
  }
  }
  context 'Methods' do
    describe "GET articles#index" do
      it "index action should be success" do
        get :index
        expect(response).to have_http_status(:success)
        expect(response).to render_template :index
      end
    end
    describe "GET articles#show" do
      it "should render articles#show template" do
        user = User.create!(valid_attributes)
        article = Article.create!(id:6 ,title:"First",description:"First Article",user_id: user.id)
        get :show, params: {id:article.id}
        expect(response).to have_http_status(:success)
        expect(response).to render_template :show
      end
    end
    describe "POST articles#create" do
      it 'create new article if logged in' do
        user = User.create!(valid_attributes)
        session[:user_id] = user.id
        expect{(post :create,  params:{article: params})}.to change(Article, :count).by(1)
      end
      it 'do not create article if not logged in' do

        expect{(post :create,  params:{article: params})}.to change(Article, :count).by(0)
      end
    end
    describe "PUT #update" do
      it "should update article info" do
        user = User.create!(valid_attributes)
        session[:user_id] = user.id
        article = Article.create!(id:6 ,title:"First",description:"First Article",user_id: user.id)
        put :update, params:{id: article.id, article: params}
        article.reload
        params.keys.each do |key|
          expect(article.attributes[key.to_s]).to eq params[key]
        end
      end
      describe 'DELETE #destroy' do
        it 'should delete article' do
          user = User.create!(valid_attributes)
          session[:user_id] = user.id
          article = Article.create!(id:6 ,title:"First",description:"First Article",user_id: user.id)
          expect { delete :destroy, params: { id: article.id } }.to change(Article, :count).by(-1)
          expect(flash[:success]).to eq "Article was deleted"
        end
      end
    end
  end

end
