require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    let(:valid_attributes) {
      {
          :username => "Foo",
          :email => "foo@bar.com",
          :password => "password",
          :password_confirmation => "password"
      }
    }
    it "validates presence of username" do
      should validate_presence_of(:username)
    end
    it "validates uniqueness (case sensitive = false) of username" do
      User.create!(valid_attributes)
      user = User.new(:username => "foo",:email => "other@bar.com", :password => "foobar",:password_confirmation => "foobar")
      expect(user).to be_invalid
    end
    it "validates length of username" do
      should validate_length_of(:username).is_at_least(3)
      should validate_length_of(:username).is_at_most(25)
    end
    it "validates presence of email" do
      should validate_presence_of(:email)
    end
    it "validates length of email" do
      should validate_length_of(:email).is_at_most(105)
    end
    it "validates uniqueness of email" do
      User.create!(valid_attributes)
      user = User.new(:username => "Bar",:email => "foo@bar.com", :password => "foobar",:password_confirmation => "foobar")
      expect(user).to be_invalid
    end
    it "requires the email to look like an email" do
      user = User.create!(valid_attributes)
      user.email = "foobar"
      expect(user).to be_invalid
    end
    it "downcases an email before saving" do
      user  = User.new(valid_attributes)
      user.email = "FOO@BAR.COM"
      expect(user.save).to be_truthy
      expect(user.email). to eq("foo@bar.com")
    end
    it "validates presence of password" do
      should validate_presence_of(:password)
    end
    it "validates length of password" do
      should validate_length_of(:password).is_at_least(8)
    end
    it "validates confirmation of password mismatch" do
      user = User.new(valid_attributes.merge(:password => "password", :password_confirmation => "wrong"))
      expect(user).to be_invalid
    end
    it "validates confrimation of password with case_sensitive" do
      user = User.new(valid_attributes.merge(:password => "password", :password_confirmation => "PASSword"))
      expect(user).to be_invalid
    end
  end
  describe "associations" do
    it do
      should have_many(:comments)
    end
    it do
      should have_many(:articles)
    end
  end
end
