Rails.application.routes.draw do
  namespace :admin do
    get 'dashboard/index'
    resources :users
    resources :articles do
      resources :comments
    end
  end

  root 'pages#index'
  get 'about', to: 'pages#about'
  get 'signup', to: 'users#new'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  #resources :comments
  resources :users, expect: [:new]
  resources :articles do
    resources :comments
  end

end
