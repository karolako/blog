class ArticlesController < ApplicationController
  before_action :set_article, only: [:edit, :update, :show, :destroy]
  def new
    if current_user
       @article = Article.new
    else
      redirect_to articles_path
    end
  end
  def create
   #render plain: current_user.id
    if current_user
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    if @article.save
      flash[:success] = "Article was successfully created!"
      redirect_to @article
    else
     render 'new'
    end
    else
      redirect_to root_path
    end
  end
  def show
    @comment = Comment.new
    @comments = @article.comments
  end
  def edit
    if current_user

    else
      redirect_to article_path(@article.id)
    end

  end
  def update
    if @article.update(article_params) && current_user.id == @article.user_id
      flash[:success] = "Article was updated"
      redirect_to @article
    else
      flash[:success] = "Article was not updated"
      render 'edit'
    end
  end
  def index
    @article = Article.all
  end
  def destroy
    if current_user && current_user.id == @article.user_id
    @article.destroy
    flash[:success] = "Article was deleted"
    redirect_to articles_path
    end
  end
  def set_article
    @article = Article.find(params[:id])
  end
  private
    def article_params
      params.require(:article).permit(:title,:description)
    end
end