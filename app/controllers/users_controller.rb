class UsersController < ApplicationController
  before_action :set_user, only: [:edit, :update, :show]
  def new
    @user = User.new
  end
  def create
    @user = User.new(user_params)
    @user.role = "user"
    if @user.save
      flash[:success] = "Welcome to the  blog #{@user.username}"
      session[:user_id] = @user.id
      redirect_to users_path(@user)
    else
      render 'new'
    end
  end
  def update
    if @user.update(user_params)
      flash[:success] = "Your account was updated successfully"
      redirect_to articles_path
    end
  end
  def edit
  end
  def show
  end
  def set_user
    @user = User.find(params[:id])
  end
  def index
    @user = User.all
  end
  private
  def user_params
    params.require(:user).permit(:username,:email,:password,:password_confirmation)
  end

end