class Admin::CommentsController < ApplicationController
  before_action :set_article, only: [:create, :update]
  def create
    @comment = @article.comments.create(comment_params)
    @comment.user_id = current_user.id
    @comment.status = "unpublished"

    if @comment.save
      flash[:success] = "Comment was added!"
      redirect_to admin_article_path(@article)

    else
      flash[:success] = "Comment was not added"
      redirect_to admin_article_path(@article)
    end
  end
  def update
    @comment = Comment.find(params[:article_id])
    if @comment.status == "published"
      @comment.update(status: "unpublished")
      flash[:success] = "Comment was published"
      redirect_to request.referrer
    else
      @comment.update(status: "published")
      flash[:success] = "Comment was unpublished"
      redirect_to request.referrer
    end
  end
  def edit
  end
  def destroy
    @comment = Comment.find(params[:article_id])
    @comment.destroy
    flash[:success] = "Comment was deleted"
    redirect_to request.referrer
  end
  def set_article
    @article = Article.find(params[:article_id])
  end
  private
  def comment_params
    params.require(:comment).permit(:comment)
  end

end