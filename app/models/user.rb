class User < ActiveRecord::Base
  has_many :articles
  has_many :comments
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :username, presence: true, uniqueness: {case_sensitive: false}, length: { minimum: 3, maximum: 25 }
  validates :email, presence: true, length: {maximum: 105}, uniqueness: true, format: {with: VALID_EMAIL_REGEX}
            before_save {self.email = email.downcase}
  validates :password, presence: true, length: {minimum: 8}
  validates :password, confirmation: { case_sensitive: true }
  has_secure_password
end