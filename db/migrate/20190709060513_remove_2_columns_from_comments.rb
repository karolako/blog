class Remove2ColumnsFromComments < ActiveRecord::Migration[5.1]
  def change
    remove_column :comments, :commentable_id
    remove_column :comments, :commentable_type
    remove_column :comments, :name
  end
end
